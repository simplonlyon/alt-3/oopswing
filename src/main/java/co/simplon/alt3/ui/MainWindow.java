package co.simplon.alt3.ui;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainWindow extends JFrame {
    
    private JLabel label =  new JLabel("Du Texte");
    private JPanel panel = new JPanel();

    public MainWindow() {
        super("My Application");
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        this.add(panel);
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void run() {
        
        
        panel.add(label);
        
        panel.add(new HelloButton("Coucou", this));
        this.setVisible(true);

    }

    public void changeText(String text) {
        label.setText(text);
    }


    
}
