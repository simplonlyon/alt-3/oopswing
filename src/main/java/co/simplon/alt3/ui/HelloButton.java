package co.simplon.alt3.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class HelloButton extends JButton implements ActionListener {
    private MainWindow window;

    public HelloButton(String text, MainWindow window) {
        super(text);
        this.window= window;
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
       System.out.println("bonjour");
       window.changeText("alt3");
        
    }
    
}
